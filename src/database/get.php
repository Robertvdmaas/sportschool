<?php

// Function getMember loads all the info from a single member from table `leden`
function getMember($db, $id): array {
    try {
        $q = $db->prepare("SELECT * FROM `leden` WHERE `ID_lid`=:id");
        $q->bindParam("id", $id);
        $q->execute();
        $member = $q->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        die("Could not get member: " . $e->getMessage());
    }
    return $member;
}

// Function getMembers loads all the members from table leden.
function getMembers($db): array {
    try {
        $q = $db->prepare("SELECT * FROM `leden`");
        $q->execute();
        $members = $q->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        die("Could not get members: " . $e->getMessage());
    }
    return $members;
}

// Function getTrainers loads all the trainers from table begeleiders.
function getTrainers($db): array {
    try {
        $q = $db->prepare("SELECT * FROM `begeleiders`");
        $q->execute();
        $trainers = $q->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        die("Could not get trainers: " . $e->getMessage());
    }
    return $trainers;
}

// Function getTrainer loads a single trainer.
function getTrainer($db, $id): array {
    try {
        $q = $db->prepare("SELECT * FROM `begeleiders` WHERE `ID_begeleider`=:id");
        $q->bindParam("id", $id);
        $q->execute();
        $trainer = $q->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        die("Could not get trainer: " . $e->getMessage());
    }
    return $trainer;
}

// Function getSport loads a single sport.
function getSport($db, $id): array {
    try {
        $q = $db->prepare("SELECT * FROM `sporten` WHERE `sportcode`=:id");
        $q->bindParam("id", $id);
        $q->execute();
        $sport = $q->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        die("Could not get sport: " . $e->getMessage());
    }
    return $sport;
}

// Function getTrainerIDByMail returns the id of a trainer with given email.
function getTrainerIDByMail($db, $mail): string {
    try {
        $q = $db->prepare("SELECT `ID_begeleider` FROM `begeleiders` WHERE `email_begeleider`=:mail");
        $q->bindParam("mail", $mail);
        $q->execute();
        $trainer = $q->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        die("Could not get trainer: " . $e->getMessage());
    }
    $id = "";
    foreach ($trainer as $trainerInfo) {
        $id =  $trainerInfo['ID_begeleider'];
    }
    return $id;
}

// Function getMemberIDByMail returns the id of a member with given email.
function getMemberIDByMail($db, $mail): string {
    try {
        $q = $db->prepare("SELECT `ID_lid` FROM `leden` WHERE `emailadres_lid`=:mail");
        $q->bindParam("mail", $mail);
        $q->execute();
        $member = $q->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        die("Could not get member: " . $e->getMessage());
    }
    $id = "";
    foreach ($member as $memberInfo) {
        $id =  $memberInfo['ID_lid'];
    }
    return $id;
}

// Function getSportTrainers loads the info of the sport trainers with the given trainer id.
function getSportTrainers($db, $trainerID): array {
    try {
        $q = $db->prepare("SELECT sportbegeleider.ID_sb, sportbegeleider.sportcode, sporten.sportnaam, sportbegeleider.sportjaar, 
                            sportbegeleider.bijzonderheden FROM `sportbegeleider` 
                            INNER JOIN sporten ON sportbegeleider.sportcode = sporten.sportcode WHERE `ID_begeleider`=:trainerID");
        $q->bindParam("trainerID", $trainerID);
        $q->execute();
        $sportTrainers = $q->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        die("Could not get sport trainers: " . $e->getMessage());
    }
    return $sportTrainers;
}

// Function getPractisedSports loads the info of the practised sport with the given member id.
function getPractisedSports($db, $memberID): array {
    try {
        $q = $db->prepare("SELECT beoefende_sporten.sportcode, sporten.sportnaam, beoefende_sporten.contributiejaar, 
                            beoefende_sporten.contributiebedrag, beoefende_sporten.betaald FROM `beoefende_sporten` 
                            INNER JOIN sporten ON beoefende_sporten.sportcode = sporten.sportcode WHERE `ID_lid`=:memberID");
        $q->bindParam("memberID", $memberID);
        $q->execute();
        $practisedSports = $q->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        die("Could not get practised sports: " . $e->getMessage());
    }
    return $practisedSports;
}

// Function getTrainers loads all the trainers from table begeleiders.
function getSports($db): array {
    try {
        $q = $db->prepare("SELECT * FROM `sporten`");
        $q->execute();
        $sports = $q->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        die("Could not get sports: " . $e->getMessage());
    }
    return $sports;
}