<!DOCTYPE html>

<head>
    <link rel="stylesheet" type="text/css" href="styles/main.css"/>
</head>

<body>
    <a class="home" href="/">Home</a>
    <a class="members" href="members_page.php">Leden</a>
    <a class="trainers" href="trainers.php">Begeleiders</a>
    <a class="sports" href="sport_page.php">Sporten</a>
</body>
