<!DOCTYPE html>

<head>
    <title>Begeleiders</title>
    <link rel="stylesheet" type="text/css" href="styles/main.css"/>
    <link rel="stylesheet" type="text/css" href="styles/trainers.css"/>
    <link rel="stylesheet" type="text/css" href="styles/table.css"/>
    <link rel="stylesheet" type="text/css" href="styles/form.css"/>
</head>

<body>
    <div class="wrapper">
        <div class="heading">
            <header>
                <h1>Begeleiders</h1>
            </header>
        </div>
        <div class="sidebar">
            <div class="sidebar-trainers">
                <?php include 'sidebar.php'; ?>
            </div>
        </div>
        <div class="content">
            <div class="content-padding">
                <div class="table-parent">
                    <table>
                        <thead>
                            <tr>
                                <th class="th-id">ID</th>
                                <th class="th-name">Naam</th>
                                <th class="th-gender">Geslacht</th>
                                <th class="th-number">Telefoonnummer</th>
                                <th class="th-mail">Email</th>
                                <th class="del-col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            include '../src/database/get.php';
                            include '../src/database/delete.php';
                            include '../src/database/update.php';
                            include '../src/database/database.php';
                            include '../src/database/create.php';

                            date_default_timezone_set("Europe/Amsterdam");
                            $db = connect();
                            $trainers = getTrainers($db);
                            foreach ($trainers as $trainer) {
                                echo "<tr>";
                                foreach ($trainer as $trainerData) {
                                    echo "<td>" . $trainerData . "</td>";
                                }
                                echo "<td class='del-col'><form class='del-form' action='' method='post'>
                                    <button id='edit-but' type='submit' name='edit' value='$trainer[ID_begeleider]'>Bewerk</button>
                                    </form></td>";
                                echo "</tr>";
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <?php
                if (isset($_POST['edit'])) {
                    $id = $_POST['edit'];
                    $trainer = getTrainer($db, $id);
                    $name = $trainer[0]['naam_begeleider'];
                    $gender = $trainer[0]['geslacht_begeleider'];
                    $tel = $trainer[0]['telefoonnummer_begeleider'];
                    $mail = $trainer[0]['email_begeleider'];
                    echo "<div class='overlay'>";
                        echo "<div class='overlay-content'>";
                            echo "<div class='flex-row'>";
                                echo "<div class='edit-form'>";
                                    echo "<form class='edit-form' action='' method='post'>";
                                        echo "<label for='name-input'>Naam</label>";
                                        echo "<input id='name-input' type='text' name='name' value='$name' placeholder='Full name' required/><br><br>";
                                        echo "<label for='gender-input'>Geslacht</label>";
                                        if ($gender == 'm') {
                                            echo "<input id='gender-input' type='radio' name='gender' value='m' checked required/>m";
                                            echo "<input id='gender-input' type='radio' name='gender' value='v' required/>v<br><br>";
                                        } else {
                                            echo "<input id='gender-input' type='radio' name='gender' value='m' required/>m";
                                            echo "<input id='gender-input' type='radio' name='gender' value='v' checked required/>v<br><br>";
                                        }
                                        echo "<label for='tel'>Telefoonnummer</label>";
                                        echo "<input id='tel' type='tel' name='tel' pattern='[0-9]{10}' value='$tel' placeholder='0612345678' required/><br><br>";
                                        echo "<label for='mail'>Email</label>";
                                        echo "<input id='mail' type='email' name='mail' value='$mail' placeholder='example@example.com' required/><br><br><br>";
                                        echo "<button id='save-but' type='submit' value='$id' name='save'>Opslaan</button>";
                                        echo "<input id='cancel-but' type='button' value='Terug' onclick='location.href=`trainers.php`'/>";
                                        echo "<button id='del-but' name='delete' value='$id' type='submit'>Verwijderen</button>";
                                    echo "</form>";
                                echo "</div>";
                                echo "<div class='new-sport-trainer-form'>";
                                    echo "<form class='new-sport-trainer-form' action='' method='post'>";
                                        echo "<label for='sport'>Sport</label>";
                                        echo "<select id='sport' name='sport'>";
                                            $sports = getSports($db);
                                            foreach ($sports as $sport) {
                                                echo "<option value='$sport[sportcode]'>$sport[sportnaam]</option>";
                                            }
                                        echo "</select><br><br>";
                                        $currentYear = date("o");
                                        $maxYear = $currentYear + 2;
                                        echo "<label for='sport-year'>Sportjaar</label>";
                                        echo "<input id='sport-year' name='sportYear' type='number' placeholder='$currentYear'
                                            min='$currentYear' max='$maxYear' required/><br><br>";
                                        echo "<label for='info'>Bijzonderheden</label>";
                                        echo "<textarea id='info' name='info' rows='4' placeholder='Bijzonderheden hier'></textarea><br><br><br>";
                                        echo "<button id='save-but' type='submit' name='save-sport-trainer' value='$id'>Opslaan</button>";
                                        echo "<input id='cancel-but' type='button' value='Terug' onclick='location.href=`trainers.php`'/>";
                                    echo "</form>";
                                echo "</div>";
                            echo "</div>";
                            echo "<div class='sport-trainer-table'>";
                                echo "<table>";
                                    echo "<thead>";
                                        echo "<tr>";
                                            echo "<th>Sportcode</th>";
                                            echo "<th>Sport</th>";
                                            echo "<th>Jaar</th>";
                                            echo "<th>Bijzonderheden</th>";
                                            echo "<th></th>";
                                        echo "</tr>";
                                    echo "</thead>";
                                    echo "<tbody>";
                                        $sportTrainers = getSportTrainers($db, $id);
                                        foreach ($sportTrainers as $sportTrainer) {
                                            $sportTrainerID = $sportTrainer['ID_sb'];
                                            $sportCode = $sportTrainer['sportcode'];
                                            $sportName = $sportTrainer['sportnaam'];
                                            $sportYear = $sportTrainer['sportjaar'];
                                            $info = $sportTrainer['bijzonderheden'];
                                            echo "<tr>";
                                                echo "<td>$sportCode</td>";
                                                echo "<td>$sportName</td>";
                                                echo "<td>$sportYear</td>";
                                                echo "<td>$info</td>";
                                                echo "<td><form action='' method='post'><button id='edit-but' type='submit'
                                                value='$sportTrainerID' name='del-sport-trainer-but'>Verwijder</button></form></td>";
                                            echo "</tr>";
                                        }
                                    echo "</tbody>";
                                echo "</table>";
                            echo "</div>";
                        echo "</div>";
                    echo "</div>";
                }
                echo "<div class='resp'>";

                function validateTrainerID($db, $trainerID): bool {
                    $trainers = getTrainers($db);

                    foreach ($trainers as $trainer) {
                        if ($trainer['ID_begeleider'] == $trainerID) {
                            return true;
                        }
                    }
                    return false;
                }

                function validateSportCode($db, $sportCode): bool {
                    $sports = getSports($db);

                    foreach ($sports as $sport) {
                        if ($sport['sportcode'] == $sportCode) {
                            return true;
                        }
                    }
                    return false;
                }

                function validateTrainer($name, $gender, $tel, $mail): bool {
                    if (($name != "") &&
                        ($gender != "") &&
                        ($gender == 'm' || $gender == 'v') &&
                        ($tel != "") &&
                        (is_numeric($tel)) &&
                        (strlen($tel) == 10) &&
                        ($mail != "") &&
                        (filter_var($mail, FILTER_VALIDATE_EMAIL))) {
                        return true;
                    } else {
                        return false;
                    }
                }

                function validateSportTrainer($db, $sportCode, $sportYear): bool {
                    $currentYear = date("o");
                    $maxYear = $currentYear + 2;
                    if (($sportCode != "") &&
                        (validateSportCode($db, $sportCode)) &&
                        ($sportYear != "") &&
                        ($sportYear >= $currentYear && $sportYear <= $maxYear)) {
                        return true;
                    } else {
                        return false;
                    }
                }

                if (isset($_POST['delete'])) {
                    $id = $_POST['delete'];
                    if (validateTrainerID($db, $id)) {
                        deleteTrainer($db, $id);
                        header("Refresh:0");
                    } else {
                        echo "ID van te verwijderen trainer niet geldig. Probeer opnieuw.";
                    }
                }

                if (isset($_POST['save'])) {
                    $id = $_POST['save'];
                    $name = $_POST['name'];
                    $gender = $_POST['gender'];
                    $tel = $_POST['tel'];
                    $mail = $_POST['mail'];
                    if (validateTrainer($name, $gender, $tel, $mail) && validateTrainerID($db, $id)) {
                        updateTrainer($db, $id, $name, $gender, $tel, $mail);
                        header("Refresh:0");
                    } else {
                        echo "Invoer niet geldig. Probeer opnieuw.";
                    }
                }

                if (isset($_POST['save-sport-trainer'])) {
                    $trainerID = $_POST['save-sport-trainer'];
                    $sportCode = $_POST['sport'];
                    $sportYear = $_POST['sportYear'];
                    $info = $_POST['info'];
                    if (validateSportTrainer($db, $sportCode, $sportYear) && validateTrainerID($db, $trainerID)) {
                        createSportTrainer($db, $trainerID, $sportCode, $sportYear, $info);
                        header("Refresh:0");
                    } else {
                        echo "Invoer niet geldig. Probeer opnieuw.";
                    }
                }

                if (isset($_POST['del-sport-trainer-but'])) {
                    $sportTrainerID = $_POST['del-sport-trainer-but'];
                    deleteSportTrainer($db, $sportTrainerID);
                }

                echo "</div>";
                ?>
                <div class="button-new">
                    <button id="new-but" onclick="location.href='new_trainer.php'">Nieuw</button>
                </div>
            </div>
        </div>
    </div>
</body>